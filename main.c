#include <stdio.h>
#include "lib/mod.h"
#include "lib/list.h"

int main(int argc, char** argv) {
    // Variable de test des iterations
    INT test = 4;

    // Variables qui seront stockees dans la liste
    INT testOne = 4;
    INT testTwo = 2;
    INT testThree = 9;
    INT testFour = 2;
    INT testFive = 4;

    // Ajout de elements dans la liste
    List* lst = NewList();
    List* lstTwo = NewList();
    lst->Prepend(lst, &testOne);
    lst->Append(lst, &testTwo);
    lstTwo->Append(lstTwo, &testThree);
    lstTwo->Append(lstTwo, &testFour);
    lstTwo->Append(lstTwo, &testFive);

    // Affichage des elements
    lst->Display(lst, LT_INT);
    lstTwo->Display(lstTwo, LT_INT);

    lst->Merge(lst, lstTwo);
    // Suppression de lstTwo une fois le merge effectue
    DELETE(lstTwo);
    lstTwo = NULL;

    lst->Display(lst, LT_INT);

    // On compte le nombre de 4 dans la liste
    printf("Count : %d\n", lst->Count(lst, &test, LT_INT));

    // On supp tt les 4 de la liste
    lst->DeleteIt(lst, &test, LT_INT, TRUE);

    // On re-affiche la liste et on re-compte le nombre de 4 dans la liste
    lst->Display(lst, LT_INT);
    printf("Count : %d\n", lst->Count(lst, &test, LT_INT));

    // Suppression de tt les elements de la liste
    lst->Purge(lst);
    DELETE(lst);
}
