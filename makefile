OUTPUT=main.out
COMPILER=gcc
ASSEMBLER=ar cr
FLAGS=-Wall -std=c99
LIBS=list.a 

all: prevent main clean test

prevent:
	$(info #################################################)
	$(info # Don't forget to create ./bin directory before #)
	$(info #################################################)

main: ${LIBS}
	${COMPILER} main.c -L. ${LIBS} -o ${OUTPUT} ${FLAGS}

clean:
	rm -rf *.a

# Start programm after compilation
test: 
	$(info #################################################)
	./${OUTPUT}

# Lib list 
list.a: list.a-element list.a-control list.a-getter
	${COMPILER} -c lib/list/list.c -o bin/list.o  ${FLAGS}
	${ASSEMBLER} list.a bin/list-*.o bin/list.o
list.a-element:
	${COMPILER} -c lib/list/element.c -o bin/list-element.o  ${FLAGS}
list.a-control:
	${COMPILER} -c lib/list/control.c -o bin/list-control.o  ${FLAGS}   
list.a-getter:
	${COMPILER} -c lib/list/getter.c -o bin/list-getter.o  ${FLAGS}   