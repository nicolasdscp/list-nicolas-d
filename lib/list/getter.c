// File: getter.c
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Implementation file fo getter.h
#include "getter.h"
#include "element.h"    // Provide Element struct

// Return the wanted element at the given index (start at 0)
Element* listGetElement(List* self, UINT idx) {
    if (idx >= self->length) return NULL;   // return NULL if idx is outside the List
    UINT currentIdx = 0;
    Element* elem = self->first;
    while (currentIdx < idx) {
        elem = elem->next;
        currentIdx++;
    }
    return elem;
}

// Call listGetElement and return only the data inside the element 
VOID* listGetData(List* self, UINT idx) {
    Element* elem = self->GetElement(self, idx);
    return elem == NULL ? NULL : elem->data;
}

// Call listGetData and return the result casted in INT
INT listGetDataInt(List* self, UINT idx) {
    return GETV(self->GetData(self, idx), INT);
}

// Call listGetData and return the result casted in LONG
LONG listGetDataLong(List* self, UINT idx) {
    return GETV(self->GetDataLong(self, idx), LONG);
}

// Call listGetData and return the result casted in FLOAT
FLOAT listGetDataFloat(List* self, UINT idx) {
    return GETV(self->GetData(self, idx), FLOAT);
}

// Call listGetData and return the result casted in DOUBLE
DOUBLE listGetDataDouble(List* self, UINT idx) {
    return GETV(self->GetData(self, idx), DOUBLE);
}

// Call listGetData and return the result casted in STRING
STRING listGetDataString(List* self, UINT idx) {
    return GETV(self->GetData(self, idx), STRING);
}