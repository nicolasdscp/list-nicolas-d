// File: control.c
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Implementation file for control.h
#include "control.h"

// Add an element at the end of the list
// @return length of list after modifications
UINT listAppend(List* self, VOID* data) {
    if (self->length == 0) {
        self->first = newElement(data, NULL, NULL);
        self->last = self->first;
    } else {
        self->last->next = newElement(data, self->last, NULL);
        self->last = self->last->next;
    }
    self->length++;
    return self->length;
}

// Delete an element with the given id
// @return length of list after modifications
UINT listDelete(List* self, UINT idx) {
    if (idx >= self->length) return self->length;
    if (idx == 0) {
        Element* toDel = self->first;
        self->first = self->first->next;
        self->first->previous = NULL;
        DELETE(toDel);
    } else {
        Element* elem = self->GetElement(self, idx);
        Element* prev = elem->previous;
        prev->next = elem->next;
        DELETE(elem);
        if (idx == self->length - 1) self->last = prev;
    }
    self->length--;
    return self->length;
}

// Insert an element at the given position
// @return length of list after modifications
UINT listInsert(List* self, VOID* data, UINT idx) {
    if (idx >= self->length) return self->length;
    if (idx == 0) return self->Prepend(self, data);
    Element* prev = self->GetElement(self, idx - 1);
    Element* elem = newElement(data, prev, prev->next);
    prev->next->previous = elem;
    prev->next = elem;
    self->length++;
    return self->length;
}

// Add an element at the start of the list
// @return length of list after modifications
UINT listPrepend(List* self, VOID* data) {
    Element* newElem = newElement(data, NULL, self->first);
    if (self->first != NULL) self->first->previous = newElem;
    else self->last = newElem;
    self->first = newElem;
    self->length++;
    return self->length;
}

// Delete all element in the list
VOID listPurge(List* self) {
    if (self->length == 0) return;
    Element* current = self->first;
    Element* next;
    do {
        next = current->next;
        DELETE(current);
        current = next;
    } while (current != NULL);
    self->first = NULL;
    self->last = NULL;
    self->length = 0;
}

// Delete all element in the list, delete also data pointed by elements
VOID listPurgeAll(List* self) {
    if (self->length == 0) return;
    Element* current = self->first;
    Element* next;
    do {
        next = current->next;
        if (current->data != NULL) DELETE(current->data);
        DELETE(current);
        current = next;
    } while (current != NULL);
    self->first = NULL;
    self->last = NULL;
    self->length = 0;
}

VOID listMerge(List* self, List* lstToAdd) {
    if (self->last != NULL) self->last->next = lstToAdd->first;
    if (lstToAdd->first != NULL) lstToAdd->first->previous = self->last;
    self->last = lstToAdd->last;
    self->length += lstToAdd->length;
}

VOID unleashElement(List* self, Element* el) {
    if (el->previous != NULL) el->previous->next = el->next;
    if (el->next != NULL) el->next->previous = el->previous;
    if (el->previous == NULL) self->first = el->next;
    if (el->next == NULL) self->last = el->previous;
    self->length--;
}