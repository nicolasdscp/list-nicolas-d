// File: getter.h
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Header file of base getting functions for List
#ifndef LIB_LIST_GETTER
#define LIB_LIST_GETTER

#include "../mod.h"     // Provide base typedef
#include "list.h"       // Provide List struct

Element* listGetElement(List* self, UINT idx);       // local function for List->GetElement

VOID*    listGetData(List* self, UINT idx);          // local function for List->GetData
INT      listGetDataInt(List* self, UINT idx);       // local function for List->GetDataInt
LONG     listGetDataLong(List* self, UINT idx);      // local function for List->GetDataLong
FLOAT    listGetDataFloat(List* self, UINT idx);     // local function for List->GetDataFloat
DOUBLE   listGetDataDouble(List* self, UINT idx);    // local function for List->GetDataDouble
STRING   listGetDataString(List* self, UINT idx);    // local function for List->GetDataString

#endif