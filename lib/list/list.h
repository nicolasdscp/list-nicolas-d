// File: list.h
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Header file for list.c
#ifndef LIB_LIST
#define LIB_LIST

#include "../mod.h"
#include "element.h"

// Define list type to pass in Display function parameters
enum ListTypes {
    LT_INT,     
    LT_LONG,
    LT_FLOAT,
    LT_DOUBLE,
    LT_STRING
};

// Unions
typedef struct List_S List;

// List_S (List) define struct to manage element like a List (dynamic array)
struct List_S {
    Element* first;     // first Element of the List
    Element* last;      // last Element of the List 
    UINT length;        // length of the List (number of Elements)

    // Control functions    
    UINT     FUNC(Append)        (List*, VOID* data);               // Append add Element at the end of the List
    UINT     FUNC(Delete)        (List*, UINT idx);                 // Delete Element at the given position (start at 0)
    UINT     FUNC(Insert)        (List*, VOID* data, UINT idx);     // Insert Element at the given postition (start at 0)
    UINT     FUNC(Prepend)       (List*, VOID* data);               // Insert an Element at the start of the List
    VOID     FUNC(Purge)         (List*);                           // Purge delete all Element of the List
    VOID     FUNC(PurgeAll)      (List*);                           // PurgeAll delete all Element and their data
    VOID     FUNC(Merge)         (List*, List* lstToAdd);           // Add the second list to the current list
    
    // Getter functions
    Element* FUNC(GetElement)    (List*, UINT idx);                 // GetElement at the given position (start at 0)
    VOID*    FUNC(GetData)       (List*, UINT idx);                 // GetData of Element at the given posistion (start at 0)
    INT      FUNC(GetDataInt)    (List*, UINT);                     // Same as GetData but return is casted as INT
    LONG     FUNC(GetDataLong)   (List*, UINT);                     // Same as GetData but return is casted as LONG
    FLOAT    FUNC(GetDataFloat)  (List*, UINT);                     // Same as GetData but return is casted as FLOAT
    DOUBLE   FUNC(GetDataDouble) (List*, UINT);                     // Same as GetData but return is casted as DOUBLE
    STRING   FUNC(GetDataString) (List*, UINT);                     // Same as GetData but return is casted as STRING

    // DeleteIt delete an element which matching with given data, if delAll is true, 
    // the function will delete all iteration else the first encounter
    UINT     FUNC(DeleteIt)      (List*, VOID* data, enum ListTypes, BOOLEAN delAll);

    // Display List element to the console
    VOID     FUNC(Display)       (List*, enum ListTypes);     

    // Count the number of iteration where the casted data is present
    UINT     FUNC(Count)         (List*, VOID* data, enum ListTypes);

    // Foreach execute a callback for each List Element, 4 parameters will be send to the callback
    // e (The element himself)
    // idx (The index of the current element start at 0)
    // stop (Address of a boolean, false by default, if it's set to true, the function will stop at the end of the current callback)
    // args (A List of parameters which can be passed to callback function)
    VOID     FUNC(Foreach)       (List*, VOID FUNC(callback) (Element* e, UINT idx, BOOLEAN* stop, List* args), List* args);   
};

// NewList create and return an instance of List
List* NewList(); 

#endif