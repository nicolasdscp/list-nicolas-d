// File: element.h
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Header file for List Elements
#ifndef LIB_LIST_ELEM
#define LIB_LIST_ELEM

#include "../mod.h"

typedef struct Element_S Element;

// Element_S (Element) define an element in a List
struct Element_S {
    VOID* data;
    Element* next;
    Element* previous;
}; 

// Create and return new Element
Element* newElement(VOID* data, Element* previous, Element* next);

#endif