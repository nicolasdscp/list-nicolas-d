// File: element.c
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Implementation file for element.h
#include "element.h"

Element* newElement(VOID* data, Element* previous, Element* next) {
    Element* el = NEW(Element);
    el->data = data;
    el->previous = previous;
    el->next = next;
    return el;
}