// File: list.c
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Implementation file for list.h
#include <stdio.h>
#include <string.h>
#include "list.h"
#include "getter.h"
#include "control.h"

VOID listForeach(List* self, VOID FUNC(callback) (Element*, UINT, BOOLEAN*, List*), List* args) {
    UINT idx = 0;
    BOOLEAN stop = FALSE;
    Element* current = self->first;
    while (!stop && current != NULL && idx < self->length) {
        FUNC(callback)(current, idx, &stop, args);
        idx++;
        current = current->next;
    }
}

VOID listDisplayForeach(Element* e, UINT idx, BOOLEAN* stop, List* args) {
    switch (GETV(args->first->data, INT)) {        // First args is the type
    case LT_INT:
        printf("\t> [%d] ==> {%d}\n", idx, GETV(e->data, INT));
        break;
    case LT_LONG:
        printf("\t> [%d] ==> {%ld}\n", idx, GETV(e->data, LONG));
        break;
    case LT_FLOAT:
        printf("\t> [%d] ==> {%f}\n", idx, GETV(e->data, FLOAT));
        break;
    case LT_DOUBLE:
        printf("\t> [%d] ==> {%lf}\n", idx, GETV(e->data, DOUBLE));
        break;
    case LT_STRING:
        printf("\t> [%d] ==> {\"%s\"}\n", idx, GETV(e->data, STRING));
        break;
    default:
        printf("\t> [%d] ==> {%p}\n", idx, e->data);
        break;
    }
}

VOID listDisplay(List* self, enum ListTypes tps) {
    List* args = NewList();
    args->Append(args, &tps);
    printf("Displaying List (%d elements):\n", self->length);
    self->Foreach(self, listDisplayForeach, args);
    args->Purge(args);
    DELETE(args);
}

// First args is the type (args->first->data)
// Second args is the data to compare (args->first->next->data)
// Third args is counter (args->first->next->next->data)
VOID listCountForeach(Element* e, UINT idx, BOOLEAN* stop, List* args) {
    switch (GETV(args->first->data, INT)) {        // First args is the type
    case LT_INT:
        if (GETV(e->data, INT) == GETV(args->first->next->data, INT)) 
            GETV(args->first->next->next->data, UINT)++;
        break;
    case LT_LONG: 
        if (GETV(e->data, LONG) == GETV(args->first->next->data, LONG)) 
            GETV(args->first->next->next->data, UINT)++;
        break;
    case LT_FLOAT:
        if (GETV(e->data, FLOAT) == GETV(args->first->next->data, FLOAT)) 
            GETV(args->first->next->next->data, UINT)++;
        break;
    case LT_DOUBLE:
        if (GETV(e->data, DOUBLE) == GETV(args->first->next->data, DOUBLE)) 
            GETV(args->first->next->next->data, UINT)++;
        break;
    case LT_STRING:
        if (strcmp(GETV(e->data, STRING), GETV(args->first->next->data, STRING)) == 0)
            GETV(args->first->next->next->data, UINT)++;
        break;
    }
}

UINT listCount(List* self, VOID* data, enum ListTypes dataType) {
    List* args = NewList();
    UINT cnt = 0;
    args->Append(args, &dataType);  // set datatype in first parameter
    args->Append(args, data);       // set data in second parameter
    args->Append(args, &cnt);       // set counter in third parameter
    self->Foreach(self, listCountForeach, args);
    args->Purge(args);
    DELETE(args);
    return cnt;
}

UINT listDeleteIt(List* self, VOID* data, enum ListTypes dataType, BOOLEAN delAll) {
    Element* current = self->first;
    Element* temp = NULL;
    BOOLEAN delThis = FALSE;
    while (current != NULL) {
        switch (dataType) {
        case LT_INT:
            if (GETV(data, INT) == GETV(current->data, INT)) delThis = TRUE;
            break;
        case LT_LONG:
            if (GETV(data, LONG) == GETV(current->data, LONG)) delThis = TRUE;
            break;
        case LT_FLOAT:
            if (GETV(data, FLOAT) == GETV(current->data, FLOAT)) delThis = TRUE;
            break;
        case LT_DOUBLE:
            if (GETV(data, DOUBLE) == GETV(current->data, DOUBLE)) delThis = TRUE;
            break;
        case LT_STRING:
            if (strcmp(GETV(data, STRING), GETV(current->data, STRING))) delThis = TRUE;
            break;
        }

        temp = current;
        current = current->next;
        if (delThis) {
            unleashElement(self, temp);
            DELETE(temp);
            if (!delAll) return self->length;
        }   
        delThis = FALSE;
    }
    return self->length;
}

List* NewList() {
    List* lst = NEW(List);
    lst->first = NULL;
    lst->last = NULL;
    lst->length = 0;

    lst->Display = listDisplay;
    lst->DeleteIt = listDeleteIt;
    lst->Count = listCount;
    lst->Foreach = listForeach;

    // Defined in control.h
    lst->Append = listAppend;
    lst->Delete = listDelete;
    lst->Insert = listInsert;
    lst->Prepend = listPrepend;
    lst->Purge = listPurge;
    lst->PurgeAll = listPurgeAll;
    lst->Merge = listMerge;

    // Defined in getter.h
    lst->GetElement = listGetElement;
    lst->GetData = listGetData;
    lst->GetDataInt = listGetDataInt;
    lst->GetDataLong = listGetDataLong;
    lst->GetDataFloat = listGetDataFloat;
    lst->GetDataDouble = listGetDataDouble;
    lst->GetDataString = listGetDataString;
    return lst;
}