// File: control.h
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: Header file for base control functions for List
#ifndef LIB_LIST_CONTROL
#define LIB_LIST_CONTROL

#include "../mod.h"
#include "list.h"

UINT listAppend(List* self, VOID* data);                // local function for List->Append
UINT listDelete(List* self, UINT idx);                  // local function for List->Delete
UINT listInsert(List* self, VOID* data, UINT idx);      // local function for List->Insert
UINT listPrepend(List* self, VOID* data);               // local function for List->Prepend
VOID listPurge(List* self);                             // local function for List->Purge
VOID listPurgeAll(List* self);                          // local function for List->PurgeAll
VOID listMerge(List* self, List* lstToAdd);             // local function for List->Merge
VOID unleashElement(List* self, Element* el);           // Unleash an Element of the list

#endif