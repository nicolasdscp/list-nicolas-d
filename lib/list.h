// File: lib/list.h
// Author: Nicolas Descamps
// Date: 2020/02/19
// Desc: List main included file, provide a direct access like #include "lib/list.h" instead of #include "lib/list/list.h"
#include "list/list.h"